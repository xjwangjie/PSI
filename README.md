# 关于PSI

PSI是企业管理软件，是由`艾格林门信息服务（大连）有限公司`发起的商业项目。

商务合作微信(只谈商务，不提供技术方面的免费咨询)

![商务合作微信](wx.jpg)

# PSI的开源协议

PSI的开源协议为GPL v3

# PSI行业衍生版

1. PSI GI - PSI服装行业版：https://gitee.com/crm8000/PSI_GI
